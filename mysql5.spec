%global pkg_name %{name}
%global pkgnamepatch mysql5

# The reason of introduce this is:
# openEuler/gcc version bump so quick. So any new gcc version might default disable
# some options which are default to enable in previos gcc versions.
%global gcc_c_adaptive_options -fcommon

%{!?runselftest:%global runselftest 1}
%global check_testsuite 0
%global with_shared_lib_major_hack 0
%global _pkgdocdirname %{pkg_name}%{!?_pkgdocdir:-%{version}}
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{pkg_name}-%{version}}
%global _hardened_build 1
%global _default_patch_flags --no-backup-if-mismatch
%global skiplist platform-specific-tests.list
%bcond_without clibrary
%bcond_without embedded
%bcond_without devel
%bcond_without client
%bcond_without common
%bcond_without errmsg
%bcond_without test
%bcond_with config
%bcond_with debug
%bcond_without init_systemd
%bcond_with init_sysv
%global daemondir %{_unitdir}
%global daemon_name mysqld
%global daemon_no_prefix mysqld
%global pidfiledir %{_rundir}/%{daemon_name}
%global logrotateddir %{_sysconfdir}/logrotate.d
%global logfiledir %{_localstatedir}/log
%global logfile %{logfiledir}/%{daemon_no_prefix}.log
%global dbdatadir %{_localstatedir}/lib/mysql
%global mysqluserhome /var/lib/mysql
%bcond_without mysql_names
%bcond_without conflicts
%global sameevr   %{?epoch:%{epoch}:}%{version}-%{release}
Name:                mysql5
Version:             5.7.43
Release:             1
Summary:             MySQL client programs and shared libraries
URL:                 http://www.mysql.com
License:             GPLv2 with exceptions and LGPLv2 and BSD
Source0:             https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-boost-%{version}.tar.gz
Source2:             mysql_config_multilib.sh
Source3:             my.cnf.in
Source7:             README.mysql-license
Source10:            mysql.tmpfiles.d.in
Source11:            mysql.service.in
Source12:            mysql-prepare-db-dir.sh
Source13:            mysql-wait-ready.sh
Source14:            mysql-check-socket.sh
Source15:            mysql-scripts-common.sh
Source16:            mysql-check-upgrade.sh
Source17:            mysql-wait-stop.sh
Source18:            mysql@.service.in
Source19:            mysql.init.in
Source30:            mysql-5.6.10-rpmlintrc
Source31:            server.cnf.in
Patch1:              %{pkgnamepatch}-install-test.patch
Patch3:              %{pkgnamepatch}-logrotate.patch
Patch4:              %{pkgnamepatch}-file-contents.patch
Patch5:              %{pkgnamepatch}-scripts.patch
Patch6:              %{pkgnamepatch}-paths.patch
Patch52:             %{pkgnamepatch}-sharedir.patch
Patch70:             %{pkgnamepatch}-5.7.9-major.patch
Patch73:             %{pkgnamepatch}-libxcrypt.patch
Patch74:             fix-innodb.innodb-fail-on-aarch64.patch
Patch75:             fix_grant_user_lock_as_root.patch
Patch76:             load_generator.py.py2_py3.patch
Patch115:            boost-1.58.0-pool.patch
Patch125:            boost-1.57.0-mpl-print.patch
Patch170:            boost-1.59.0-log.patch
Patch180:            boost-1.59-python-make_setter.patch
Patch181:            boost-1.59-test-fenv.patch
Patch182:            skiptest-auth_sec.keyring_file_data_qa.patch
Patch183:            disable-main.func_math.patch
BuildRequires:       cmake gcc-c++ libaio-devel libedit-devel libevent-devel lz4-devel mecab-devel
%ifnarch aarch64 %{arm}
BuildRequires:       numactl-devel
%endif
BuildRequires:       compat-openssl11-devel perl-interpreter perl-generators rpcgen libtirpc-devel
BuildRequires:       systemtap-sdt-devel zlib-devel multilib-rpm-config procps time
BuildRequires:       perl(Digest::file) perl(Digest::MD5) perl(Env) perl(Exporter) perl(Fcntl)
BuildRequires:       perl(File::Temp) perl(Data::Dumper) perl(Getopt::Long) perl(IPC::Open3)
BuildRequires:       perl(JSON) perl(Memoize) perl(Socket) perl(Sys::Hostname) perl(Test::More)
BuildRequires:       perl(Time::HiRes) vim git grep
%{?with_init_systemd:BuildRequires: systemd}
Requires:            bash coreutils grep %{name}-common = %{sameevr}
Provides:            bundled(boost) = 1.59
%if %{with mysql_names}
Provides:            mysql = %{sameevr}
Provides:            mysql = %{sameevr}
Provides:            mysql-compat-client = %{sameevr}
Provides:            mysql-compat-client = %{sameevr}
%endif
%{?with_conflicts:Conflicts:        mariadb}
Obsoletes:           mysql-cluster < 5.1.44
%global __requires_exclude ^perl\\((hostnames|lib::mtr|lib::v1|mtr_|My::)
%global __provides_exclude_from ^(%{_datadir}/(mysql|mysql-test)/.*|%{_libdir}/mysql/plugin/.*\\.so)$
%description
MySQL is a multi-user, multi-threaded SQL database server. MySQL is a
client/server implementation consisting of a server daemon (mysqld)
and many different client programs and libraries. The base package
contains the standard MySQL client programs and generic MySQL files.
%if %{with clibrary}

%package          libs
Summary:             The shared libraries required for MySQL clients
Requires:            %{name}-common = %{sameevr}
%if %{with mysql_names}
Provides:            mysql-libs = %{sameevr}
Provides:            mysql-libs = %{sameevr}
%endif
%description      libs
The mysql-libs package provides the essential shared libraries for any
MySQL client program or interface. You will need to install this package
to use any other MySQL package or any clients that need to connect to a
MySQL server.
%endif
%if %{with config}

%package          config
Summary:             The config files required by server and client
%description      config
The package provides the config file my.cnf and my.cnf.d directory used by any
MariaDB or MySQL program. You will need to install this package to use any
other MariaDB or MySQL package if the config files are not provided in the
package itself.
%endif
%if %{with common}

%package          common
Summary:             The shared files required for MySQL server and client
Requires:            %{_sysconfdir}/my.cnf
%description      common
The mysql-common package provides the essential shared files for any
MySQL program. You will need to install this package to use any other
MySQL package.
%endif
%if %{with errmsg}

%package          errmsg
Summary:             The error messages files required by server and embedded
Requires:            %{name}-common = %{sameevr}
%description      errmsg
The package provides error messages files for the MySQL daemon and the
embedded server. You will need to install this package to use any of those
MySQL packages.
%endif

%package          server
Summary:             The MySQL server and related files
Suggests:            %{name} = %{sameevr}
Requires:            mysql
Requires:            %{name}-common = %{sameevr} %{_sysconfdir}/my.cnf
Requires:            %{_sysconfdir}/my.cnf.d %{name}-errmsg = %{sameevr}
%{?mecab:Requires: mecab-ipadic}
Requires:            coreutils
Requires(pre):    shadow
%if %{with init_systemd}
Requires:            systemd
%{?systemd_requires: %systemd_requires}
%endif
%if %{with mysql_names}
Provides:            mysql-server = %{sameevr}
Provides:            mysql-server = %{sameevr}
Provides:            mysql-compat-server = %{sameevr}
Provides:            mysql-compat-server = %{sameevr}
Obsoletes:           mysql-bench < 5.7.8
%endif
Obsoletes:           mysql5-bench < 5.7.8
%{?with_conflicts:Conflicts:        mariadb-server}
%{?with_conflicts:Conflicts:        mariadb-galera-server}
Obsoletes:           mariadb-server-utils < 3:10.1.21-3
%description      server
MySQL is a multi-user, multi-threaded SQL database server. MySQL is a
client/server implementation consisting of a server daemon (mysqld)
and many different client programs and libraries. This package contains
the MySQL server and some accompanying files and directories.
%if %{with devel}

%package          devel
Summary:             Files for development of MySQL applications
%{?with_clibrary:Requires:         %{name}-libs = %{sameevr}}
Requires:            pkgconfig(openssl) zlib-devel
%{?with_conflicts:Conflicts:        mariadb-devel}
%description      devel
MySQL is a multi-user, multi-threaded SQL database server. This
package contains the libraries and header files that are needed for
developing MySQL client applications.
%endif
%if %{with embedded}

%package          embedded
Summary:             MySQL as an embeddable library
Requires:            %{name}-common = %{sameevr} %{name}-errmsg = %{sameevr}
%if %{with mysql_names}
Provides:            mysql-embedded = %{sameevr}
Provides:            mysql-embedded = %{sameevr}
%endif
%description      embedded
MySQL is a multi-user, multi-threaded SQL database server. This
package contains a version of the MySQL server that can be embedded
into a client application instead of running as a separate process.

%package          embedded-devel
Summary:             Development files for MySQL as an embeddable library
Requires:            %{name}-embedded = %{sameevr} %{name}-devel = %{sameevr}
Requires:            libaio-devel lz4-devel pkgconfig(openssl) zlib-devel
%{?with_conflicts:Conflicts:        mariadb-embedded-devel}
%description      embedded-devel
MySQL is a multi-user, multi-threaded SQL database server. This
package contains files needed for developing and testing with
the embedded version of the MySQL server.
%endif
%if %{with test}

%package          test
Summary:             The test suite distributed with MySQL
Requires:            %{name} = %{sameevr} %{name}-common = %{sameevr}
Requires:            %{name}-server = %{sameevr} perl(Digest::file) perl(Digest::MD5)
Requires:            perl(Env) perl(Exporter) perl(Fcntl) perl(File::Temp) perl(Data::Dumper)
Requires:            perl(Getopt::Long) perl(IPC::Open3) perl(JSON) perl(Socket) perl(Sys::Hostname)
Requires:            perl(Test::More) perl(Time::HiRes)
%{?with_conflicts:Conflicts:        mariadb-test}
%if %{with mysql_names}
Provides:            mysql-test = %{sameevr}
Provides:            mysql-test = %{sameevr}
%endif
%description      test
MySQL is a multi-user, multi-threaded SQL database server. This
package contains the regression test suite distributed with
the MySQL sources.
%endif

%prep
%setup -q -n mysql-%{version}
%patch1 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
#%patch7 -p1
%patch52 -p1
%if %{with_shared_lib_major_hack}
%patch70 -p1
%endif
%patch73 -p1
%patch74 -p1
%patch75 -p1
%patch76 -p1
%patch182 -p1
%patch183 -p1
pushd boost/boost_1_59_0
%patch115 -p0
%patch125 -p1
%patch170 -p2
%patch180 -p2
%patch181 -p2
popd
pushd mysql-test
add_test () {
    echo "$@" $ >> %{skiplist}
}
touch %{skiplist}
add_test auth_sec.openssl_cert_validity   : 2018 new year issue
%ifarch %arm
add_test innodb_fts.opt                   : arm32 FTS issue
add_test perfschema.func_file_io          : missing hw on arm32
add_test perfschema.setup_objects         : missing hw on arm32
add_test perfschema.memory_aggregate_no_a
%endif
%ifarch x86_64
add_test main.mysql_upgrade               : Result content mismatch
%endif
add_test main.grant_alter_user_qa         : ssl test disable
add_test auth_sec.mysql_ssl_connection    : ssl test disable
add_test main.events_bugs                 : ssl test disable
add_test main.ssl_crl                     : ssl test disable
add_test main.func_like                   : ssl test disable
add_test main.grant_user_lock_qa          : ssl test disable
add_test main.events_1                    : ssl test disable
add_test main.ssl_cipher                  : ssl test disable
add_test main.ssl                         : ssl test disable
add_test auth_sec.ssl_mode                : ssl test disable
add_test main.openssl_1                   : ssl test disable
add_test auth_sec.tls                     : ssl test disable
add_test binlog.binlog_grant_alter_user   : ssl test disable
add_test auth_sec.cert_verify             : ssl test disable
add_test main.mysqldump                   : ssl test disable
add_test main.ssl_8k_key                  : ssl test disable
add_test auth_sec.ssl_auto_detect         : ssl test disable
add_test main.ssl_ca                      : ssl test disable
add_test auth_sec.openssl_cert_generation : ssl test disable
add_test main.ssl_compress                : ssl test disable
add_test main.plugin_auth_sha256_tls      : ssl test disable
add_test main.mysql_ssl_default           : ssl test disable
add_test innodb.innodb-multiple-tablespaces : fail on glibc 2.33
add_test innodb.alter_kill                  : fail on glibc 2.33
add_test main.xa_prepared_binlog_off
add_test binlog.binlog_xa_prepared_disconnect
add_test rpl.rpl_semi_sync_turn_on_off_optimize_for_static_plugin_config
add_test rpl.rpl_row_until
add_test auth_sec.keyring_file_data_qa
add_test main.index_merge_delete
add_test sys_vars.innodb_buffer_pool_load_now_basic
popd
cp %{SOURCE2} %{SOURCE3} %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} \
   %{SOURCE14} %{SOURCE15} %{SOURCE16} %{SOURCE17} %{SOURCE18} %{SOURCE19} %{SOURCE31} scripts
%build
%if %runselftest
    if [ x"$(id -u)" = "x0" ]; then
        echo "mysql's regression tests fail if run as root."
        echo "If you really need to build the RPM as root, use"
        echo "--nocheck to skip the regression tests."
        exit 1
    fi
%endif
mkdir -p build && pushd build
cmake .. \
         -DBUILD_CONFIG=mysql_release \
         -DFEATURE_SET="community" \
         -DINSTALL_LAYOUT=RPM \
         -DDAEMON_NAME="%{daemon_name}" \
         -DDAEMON_NO_PREFIX="%{daemon_no_prefix}" \
         -DLOG_LOCATION="%{logfile}" \
         -DPID_FILE_DIR="%{pidfiledir}" \
         -DNICE_PROJECT_NAME="MySQL" \
         -DCMAKE_INSTALL_PREFIX="%{_prefix}" \
         -DSYSCONFDIR="%{_sysconfdir}" \
         -DSYSCONF2DIR="%{_sysconfdir}/my.cnf.d" \
         -DINSTALL_DOCDIR="share/doc/%{_pkgdocdirname}" \
         -DINSTALL_DOCREADMEDIR="share/doc/%{_pkgdocdirname}" \
         -DINSTALL_INCLUDEDIR=include/mysql \
         -DINSTALL_INFODIR=share/info \
         -DINSTALL_LIBDIR="%{_lib}/mysql" \
         -DINSTALL_MANDIR=share/man \
         -DINSTALL_MYSQLSHAREDIR=share/%{pkg_name} \
         -DINSTALL_MYSQLTESTDIR=share/mysql-test \
         -DINSTALL_PLUGINDIR="%{_lib}/mysql/plugin" \
         -DINSTALL_SBINDIR=libexec \
         -DINSTALL_SCRIPTDIR=bin \
         -DINSTALL_SUPPORTFILESDIR=share/%{pkg_name} \
         -DMYSQL_DATADIR="%{dbdatadir}" \
         -DMYSQL_UNIX_ADDR="/var/lib/mysql/mysql.sock" \
         -DENABLED_LOCAL_INFILE=ON \
         -DENABLE_DTRACE=ON \
%if %{with init_systemd}
         -DWITH_SYSTEMD=1 \
         -DSYSTEMD_SERVICE_NAME="%{daemon_name}" \
         -DSYSTEMD_PID_DIR="%{pidfiledir}" \
%endif
         -DWITH_INNODB_MEMCACHED=ON \
         -DWITH_EMBEDDED_SERVER=ON \
         -DWITH_EMBEDDED_SHARED_LIBRARY=ON \
         -DWITH_EDITLINE=system \
         -DWITH_LIBEVENT=system \
         -DWITH_LZ4=system \
         -DWITH_MECAB=system \
         -DWITH_SSL=system \
         -DWITH_ZLIB=system \
         -DWITH_BOOST=../boost \
         -DCMAKE_C_FLAGS="%{optflags}%{?with_debug: -fno-strict-overflow -Wno-unused-result -Wno-unused-function -Wno-unused-but-set-variable} %{gcc_c_adaptive_options}" \
         -DCMAKE_CXX_FLAGS="%{optflags}%{?with_debug: -fno-strict-overflow -Wno-unused-result -Wno-unused-function -Wno-unused-but-set-variable}" \
%{?with_debug: -DWITH_DEBUG=1}\
         -DTMPDIR=/var/tmp \
         %{?_hardened_build:-DWITH_MYSQLD_LDFLAGS="-pie -Wl,-z,relro,-z,now"}
make %{?_smp_mflags} VERBOSE=1
popd

%install
pushd build
make DESTDIR=%{buildroot} install
%multilib_fix_c_header --file %{_includedir}/mysql/my_config.h
if %multilib_capable; then
mv %{buildroot}%{_bindir}/mysql_config %{buildroot}%{_bindir}/mysql_config-%{__isa_bits}
install -p -m 0755 scripts/mysql_config_multilib %{buildroot}%{_bindir}/mysql_config
fi
install -p -m 0644 Docs/INFO_SRC %{buildroot}%{_libdir}/mysql/
install -p -m 0644 Docs/INFO_BIN %{buildroot}%{_libdir}/mysql/
mkdir -p %{buildroot}%{logfiledir}
mkdir -p %{buildroot}%{pidfiledir}
install -p -m 0755 -d %{buildroot}%{dbdatadir}
install -p -m 0750 -d %{buildroot}%{_localstatedir}/lib/mysql-files
install -p -m 0700 -d %{buildroot}%{_localstatedir}/lib/mysql-keyring
%if %{with config}
install -D -p -m 0644 scripts/my.cnf %{buildroot}%{_sysconfdir}/my.cnf
%endif
%if %{with init_systemd}
install -D -p -m 644 scripts/mysql.service %{buildroot}%{_unitdir}/%{daemon_name}.service
install -D -p -m 644 scripts/mysql@.service %{buildroot}%{_unitdir}/%{daemon_name}@.service
install -D -p -m 0644 scripts/mysql.tmpfiles.d %{buildroot}%{_tmpfilesdir}/%{daemon_name}.conf
rm -r %{buildroot}%{_tmpfilesdir}/mysql.conf
%endif
%if %{with init_sysv}
install -D -p -m 755 scripts/mysql.init %{buildroot}%{daemondir}/%{daemon_name}
install -p -m 755 scripts/mysql-wait-ready %{buildroot}%{_libexecdir}/mysql-wait-ready
%endif
install -p -m 755 scripts/mysql-prepare-db-dir %{buildroot}%{_libexecdir}/mysql-prepare-db-dir
install -p -m 755 scripts/mysql-wait-stop %{buildroot}%{_libexecdir}/mysql-wait-stop
install -p -m 755 scripts/mysql-check-socket %{buildroot}%{_libexecdir}/mysql-check-socket
install -p -m 755 scripts/mysql-check-upgrade %{buildroot}%{_libexecdir}/mysql-check-upgrade
install -p -m 644 scripts/mysql-scripts-common %{buildroot}%{_libexecdir}/mysql-scripts-common
install -D -p -m 0644 scripts/server.cnf %{buildroot}%{_sysconfdir}/my.cnf.d/%{pkg_name}-server.cnf
mv %{buildroot}%{_datadir}/mysql-test/lib/My/SafeProcess/my_safe_process %{buildroot}%{_bindir}
ln -s ../../../../../bin/my_safe_process %{buildroot}%{_datadir}/mysql-test/lib/My/SafeProcess/my_safe_process
rm %{buildroot}%{_bindir}/mysql_embedded
rm %{buildroot}%{_libdir}/mysql/*.a
rm %{buildroot}%{_datadir}/%{pkg_name}/magic
rm %{buildroot}%{_datadir}/%{pkg_name}/mysql.server
rm %{buildroot}%{_datadir}/%{pkg_name}/mysqld_multi.server
rm %{buildroot}%{_mandir}/man1/comp_err.1*
mkdir -p %{buildroot}%{logrotateddir}
mv %{buildroot}%{_datadir}/%{pkg_name}/mysql-log-rotate %{buildroot}%{logrotateddir}/%{daemon_name}
chmod 644 %{buildroot}%{logrotateddir}/%{daemon_name}
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/mysql" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
%if %{with debug}
mv %{buildroot}%{_libexecdir}/mysqld-debug %{buildroot}%{_libexecdir}/mysqld
%endif
popd
install -p -m 0644 %{SOURCE7} %{basename:%{SOURCE7}}
install -p -m 0644 mysql-test/%{skiplist} %{buildroot}%{_datadir}/mysql-test
%if %{without clibrary}
unlink %{buildroot}%{_libdir}/mysql/libmysqlclient.so
rm -r %{buildroot}%{_libdir}/mysql/libmysqlclient*.so.*
rm -r %{buildroot}%{_sysconfdir}/ld.so.conf.d
%endif
%if %{without embedded}
rm %{buildroot}%{_libdir}/mysql/libmysqld.so*
rm %{buildroot}%{_bindir}/{mysql_client_test_embedded,mysqltest_embedded}
rm %{buildroot}%{_mandir}/man1/{mysql_client_test_embedded,mysqltest_embedded}.1*
%endif
%if %{without devel}
rm %{buildroot}%{_bindir}/mysql_config*
rm -r %{buildroot}%{_includedir}/mysql
rm %{buildroot}%{_datadir}/aclocal/mysql.m4
rm %{buildroot}%{_libdir}/pkgconfig/mysqlclient.pc
rm %{buildroot}%{_libdir}/mysql/libmysqlclient*.so
rm %{buildroot}%{_mandir}/man1/mysql_config.1*
%endif
%if %{without client}
rm %{buildroot}%{_bindir}/{mysql,mysql_config_editor,\
mysql_plugin,mysqladmin,mysqlbinlog,\
mysqlcheck,mysqldump,mysqlpump,mysqlimport,mysqlshow,mysqlslap,my_print_defaults}
rm %{buildroot}%{_mandir}/man1/{mysql,mysql_config_editor,\
mysql_plugin,mysqladmin,mysqlbinlog,\
mysqlcheck,mysqldump,mysqlpump,mysqlimport,mysqlshow,mysqlslap,my_print_defaults}.1*
%endif
%if %{with config}
mkdir -p %{buildroot}%{_sysconfdir}/my.cnf.d
%else
%endif
%if %{without common}
rm -r %{buildroot}%{_datadir}/%{pkg_name}/charsets
%endif
%if %{without errmsg}
rm %{buildroot}%{_datadir}/%{pkg_name}/errmsg-utf8.txt
rm -r %{buildroot}%{_datadir}/%{pkg_name}/{english,bulgarian,czech,danish,dutch,estonian,\
french,german,greek,hungarian,italian,japanese,korean,norwegian,norwegian-ny,\
polish,portuguese,romanian,russian,serbian,slovak,spanish,swedish,ukrainian}
%endif
%if %{without test}
rm %{buildroot}%{_bindir}/{mysql_client_test,mysqlxtest,my_safe_process}
rm -r %{buildroot}%{_datadir}/mysql-test
rm %{buildroot}%{_mandir}/man1/mysql_client_test.1*
%endif

%check
%if %{with test}
%if %runselftest
pushd build
make test VERBOSE=1
pushd mysql-test
cp ../../mysql-test/%{skiplist} .
export MTR_BUILD_THREAD=%{__isa_bits}
./mtr \
  --mem --parallel=auto --force --retry=2 \
  --suite-timeout=720 --testcase-timeout=30 \
  --report-unstable-tests --clean-vardir \
%if %{check_testsuite}
  --max-test-fail=0 || :
%else
  --skip-test-list=%{skiplist}
%endif
  rm -r var $(readlink var)
popd
popd
%endif
%endif

%pre server
/usr/sbin/groupadd -g 27 -o -r mysql >/dev/null 2>&1 || :
/usr/sbin/useradd -M -N -g mysql -o -r -d %{mysqluserhome} -s /sbin/nologin \
  -c "MySQL Server" -u 27 mysql >/dev/null 2>&1 || :
%if %{with clibrary}
%ldconfig_post libs
%endif
%if %{with embedded}
%ldconfig_post embedded
%endif

%post server
%if %{with init_systemd}
%systemd_post %{daemon_name}.service
%endif
%if %{with init_sysv}
if [ $1 = 1 ]; then
    /sbin/chkconfig --add %{daemon_name}
fi
%endif
if [ ! -e "%{logfile}" -a ! -h "%{logfile}" ] ; then
    install /dev/null -m0640 -omysql -gmysql "%{logfile}"
fi

%preun server
%if %{with init_systemd}
%systemd_preun %{daemon_name}.service
%endif
%if %{with init_sysv}
if [ $1 = 0 ]; then
    /sbin/service %{daemon_name} stop >/dev/null 2>&1
    /sbin/chkconfig --del %{daemon_name}
fi
%endif
%if %{with clibrary}
%ldconfig_postun libs
%endif
%if %{with embedded}
%ldconfig_postun embedded
%endif

%postun server
%if %{with init_systemd}
%systemd_postun_with_restart %{daemon_name}.service
%endif
%if %{with init_sysv}
if [ $1 -ge 1 ]; then
    /sbin/service %{daemon_name} condrestart >/dev/null 2>&1 || :
fi
%endif
%if %{with client}

%files
%{_bindir}/mysql
%{_bindir}/mysql_config_editor
%{_bindir}/mysql_plugin
%{_bindir}/mysqladmin
%{_bindir}/mysqlbinlog
%{_bindir}/mysqlcheck
%{_bindir}/mysqldump
%{_bindir}/mysqlimport
%{_bindir}/mysqlpump
%{_bindir}/mysqlshow
%{_bindir}/mysqlslap
%{_mandir}/man1/mysql.1*
%{_mandir}/man1/mysql_config_editor.1*
%{_mandir}/man1/mysql_plugin.1*
%{_mandir}/man1/mysqladmin.1*
%{_mandir}/man1/mysqlbinlog.1*
%{_mandir}/man1/mysqlcheck.1*
%{_mandir}/man1/mysqldump.1*
%{_mandir}/man1/mysqlimport.1*
%{_mandir}/man1/mysqlpump.1*
%{_mandir}/man1/mysqlshow.1*
%{_mandir}/man1/mysqlslap.1*
%endif
%if %{with clibrary}

%files libs
%{_libdir}/mysql/libmysqlclient*.so.*
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/*
%endif
%if %{with config}

%files config
%dir %{_sysconfdir}/my.cnf.d
%config(noreplace) %{_sysconfdir}/my.cnf
%endif
%if %{with common}

%files common
%doc README README.mysql-license LICENSE
%doc storage/innobase/COPYING.Percona storage/innobase/COPYING.Google
%dir %{_libdir}/mysql
%dir %{_datadir}/%{pkg_name}
%{_datadir}/%{pkg_name}/charsets
%endif
%if %{with errmsg}

%files errmsg
%{_datadir}/%{pkg_name}/errmsg-utf8.txt
%{_datadir}/%{pkg_name}/english
%lang(bg) %{_datadir}/%{pkg_name}/bulgarian
%lang(cs) %{_datadir}/%{pkg_name}/czech
%lang(da) %{_datadir}/%{pkg_name}/danish
%lang(nl) %{_datadir}/%{pkg_name}/dutch
%lang(et) %{_datadir}/%{pkg_name}/estonian
%lang(fr) %{_datadir}/%{pkg_name}/french
%lang(de) %{_datadir}/%{pkg_name}/german
%lang(el) %{_datadir}/%{pkg_name}/greek
%lang(hu) %{_datadir}/%{pkg_name}/hungarian
%lang(it) %{_datadir}/%{pkg_name}/italian
%lang(ja) %{_datadir}/%{pkg_name}/japanese
%lang(ko) %{_datadir}/%{pkg_name}/korean
%lang(no) %{_datadir}/%{pkg_name}/norwegian
%lang(no) %{_datadir}/%{pkg_name}/norwegian-ny
%lang(pl) %{_datadir}/%{pkg_name}/polish
%lang(pt) %{_datadir}/%{pkg_name}/portuguese
%lang(ro) %{_datadir}/%{pkg_name}/romanian
%lang(ru) %{_datadir}/%{pkg_name}/russian
%lang(sr) %{_datadir}/%{pkg_name}/serbian
%lang(sk) %{_datadir}/%{pkg_name}/slovak
%lang(es) %{_datadir}/%{pkg_name}/spanish
%lang(sv) %{_datadir}/%{pkg_name}/swedish
%lang(uk) %{_datadir}/%{pkg_name}/ukrainian
%endif

%files server
%{_bindir}/myisamchk
%{_bindir}/myisam_ftdump
%{_bindir}/myisamlog
%{_bindir}/myisampack
%{_bindir}/my_print_defaults
%{_bindir}/mysql_install_db
%{_bindir}/mysql_secure_installation
%{_bindir}/mysql_ssl_rsa_setup
%{_bindir}/mysql_tzinfo_to_sql
%{_bindir}/mysql_upgrade
%if %{with init_systemd}
%{_bindir}/mysqld_pre_systemd
%else
%{_bindir}/mysqld_multi
%{_bindir}/mysqld_safe
%endif
%{_bindir}/mysqldumpslow
%{_bindir}/innochecksum
%{_bindir}/perror
%{_bindir}/replace
%{_bindir}/resolve_stack_dump
%{_bindir}/resolveip
%{_bindir}/lz4_decompress
%{_bindir}/zlib_decompress
%config(noreplace) %{_sysconfdir}/my.cnf.d/%{pkg_name}-server.cnf
%{_libexecdir}/mysqld
%{_libdir}/mysql/INFO_SRC
%{_libdir}/mysql/INFO_BIN
%if %{without common}
%dir %{_datadir}/%{pkg_name}
%endif
%{_libdir}/mysql/plugin
%{_mandir}/man1/myisamchk.1*
%{_mandir}/man1/myisamlog.1*
%{_mandir}/man1/myisampack.1*
%{_mandir}/man1/myisam_ftdump.1*
%{_mandir}/man1/mysql.server.1*
%{_mandir}/man1/my_print_defaults.1*
%{_mandir}/man1/mysql_install_db.1*
%{_mandir}/man1/mysql_secure_installation.1*
%{_mandir}/man1/mysql_ssl_rsa_setup.1*
%{_mandir}/man1/mysql_tzinfo_to_sql.1*
%{_mandir}/man1/mysql_upgrade.1*
%{_mandir}/man1/mysqldumpslow.1*
%if %{with init_systemd}
%exclude %{_mandir}/man1/mysqld_multi.1*
%exclude %{_mandir}/man1/mysqld_safe.1*
%else
%{_mandir}/man1/mysqld_multi.1*
%{_mandir}/man1/mysqld_safe.1*
%endif
%{_mandir}/man1/mysqlman.1*
%{_mandir}/man1/innochecksum.1*
%{_mandir}/man1/perror.1*
%{_mandir}/man1/replace.1*
%{_mandir}/man1/resolve_stack_dump.1*
%{_mandir}/man1/resolveip.1*
%{_mandir}/man1/lz4_decompress.1*
%{_mandir}/man1/zlib_decompress.1*
%{_mandir}/man8/mysqld.8*
%{_datadir}/%{pkg_name}/dictionary.txt
%{_datadir}/%{pkg_name}/fill_help_tables.sql
%{_datadir}/%{pkg_name}/innodb_memcached_config.sql
%{_datadir}/%{pkg_name}/install_rewriter.sql
%{_datadir}/%{pkg_name}/mysql_security_commands.sql
%{_datadir}/%{pkg_name}/mysql_sys_schema.sql
%{_datadir}/%{pkg_name}/mysql_system_tables.sql
%{_datadir}/%{pkg_name}/mysql_system_tables_data.sql
%{_datadir}/%{pkg_name}/mysql_test_data_timezone.sql
%{_datadir}/%{pkg_name}/uninstall_rewriter.sql
%{daemondir}/%{daemon_name}*
%{_libexecdir}/mysql-prepare-db-dir
%if %{with init_sysv}
%{_libexecdir}/mysql-wait-ready
%endif
%{_libexecdir}/mysql-wait-stop
%{_libexecdir}/mysql-check-socket
%{_libexecdir}/mysql-check-upgrade
%{_libexecdir}/mysql-scripts-common
%{?with_init_systemd:%{_tmpfilesdir}/%{daemon_name}.conf}
%attr(0755,mysql,mysql) %dir %{dbdatadir}
%attr(0750,mysql,mysql) %dir %{_localstatedir}/lib/mysql-files
%attr(0700,mysql,mysql) %dir %{_localstatedir}/lib/mysql-keyring
%attr(0755,mysql,mysql) %dir %{pidfiledir}
%attr(0640,mysql,mysql) %config %ghost %verify(not md5 size mtime) %{logfile}
%config(noreplace) %{logrotateddir}/%{daemon_name}
%if %{with devel}

%files devel
%{_bindir}/mysql_config*
%exclude %{_bindir}/mysql_config_editor
%{_includedir}/mysql
%{_datadir}/aclocal/mysql.m4
%{_datadir}/info/mysql.info.gz
%if %{with clibrary}
%{_libdir}/mysql/libmysqlclient.so
%endif
%{_libdir}/pkgconfig/mysqlclient.pc
%{_mandir}/man1/mysql_config.1*
%endif
%if %{with embedded}

%files embedded
%{_libdir}/mysql/libmysqld.so.*

%files embedded-devel
%{_libdir}/mysql/libmysqld.so
%endif
%if %{with test}

%files test
%{_bindir}/mysql_client_test
%{_bindir}/mysql_client_test_embedded
%{_bindir}/mysqltest
%{_bindir}/mysqltest_embedded
%{_bindir}/mysqlxtest
%{_bindir}/my_safe_process
%attr(-,mysql,mysql) %{_datadir}/mysql-test
%endif

%changelog
* Tue Aug  1 2023 dillon chen <dillon.chen@gmail.com> - 5.7.43-1
- Upgrade to 5.7.43 version

* Thu Jul 20 2023 dillon chen <dillon.chen@gmail.com> - 5.7.42-1
- Upgrade to 5.7.42 version

* Wed Jul 19 2023 dillon chen <dillon.chen@gmail.com> - 5.7.41-1
- Upgrade to 5.7.41 version

* Tue Jul 11 2023 dillon chen <dillon.chen@gmail.com> - 5.7.40-1
- Upgrade to 5.7.40 version

* Mon Jul 10 2023 dillon chen <dillon.chen@gmail.com> - 5.7.39-2
- ssl to compat_openssl11 
- skip auth_sec.keyring_file_data_qa test

* Thu Sep 8 2022 dillon chen <dillon.chen@gmail.com> - 5.7.39-1
- Upgrade to 5.7.39 version

* Sat May 7 2022 dillon chen <dillon.chen@gmail.com> - 5.7.38-1
- add patch76
- Upgrade to 5.7.38 version

* Thu Apr 21 2022 bzhaoop <bzhaojyathousandy@gmail.com> - 5.7.37-1
- Fix CVE issues for bump to the 5.7.37 version
- CVE-2022-21245
- CVE-2022-21270
- CVE-2022-21303
- CVE-2022-21304
- CVE-2022-21344
- CVE-2022-21367
- CVE-2021-35624
- CVE-2021-2356
- CVE-2021-2011
- CVE-2021-2010
- CVE-2021-2007

* Fri Feb 25 2022 houyingchao <houyingchao@huawei.com> - 5.7.34-2
- Upgrade to 5.7.34 version
- Fix CVES

* Thu Jul 1 2021 bzhaoop <bzhaojyathousandy@gmail.com> - 5.7.34-1
- Bump from 5.7.21 to latest 5.7.34
- Correct the test steps and options
- Refactor part of the file structure according to the new packages

* Tue Nov 30 2021 wulei <wulei80@huawei.com> - 5.7.21-4
- Fix innodb.innodb fail on aarch64
- Fix grant_user_lock as root
- Update fail tests list on glibc 2.33

* Tue Nov 24 2020 baizhonggui <baizhonggui@huawei.com> - 5.7.21-3
- Fix test error

* Mon Nov 2 2020 baizhonggui <baizhonggui@huawei.com> - 5.7.21-2
- Delete unused mode

* Thu Oct 15 2020 zhanghua <zhanghua40@huawei.com> - 5.7.21-1
- package init

